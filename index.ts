import express, { Express, Request, Response } from "express";
import dotenv from "dotenv";

//configuration .env
dotenv.config();

//Create Exress app
const app: Express = express();
const port: string | number = process.env.PORT || 8000;

//Define the first route
app.get('/', (req: Request, res: Response)=>{
    res.send('Hola typescript')
})

app.get('/hello', (req: Request, res: Response)=>{
    res.send('Welcome to GET router: Hello!')
})

app.listen(process.env.PORT, ()=>{
    console.log(`Server from: http://localhost:8000`)
})
